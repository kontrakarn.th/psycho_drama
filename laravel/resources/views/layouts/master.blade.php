
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="csrf-token" content="{{ csrf_token() }}"/>
	<title></title>

    <!-- Global stylesheets -->
    <link rel="stylesheet" href="http://demo.expertphp.in/css/jquery.treeview.css" />
    <script src="http://demo.expertphp.in/js/jquery.js"></script>
    <script src="http://demo.expertphp.in/js/jquery-treeview.js"></script>
    <script type="text/javascript" src="http://demo.expertphp.in/js/demo.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>

	<link href="{{ asset('assets/limitless/css/icons/icomoon/styles.css') }}" rel="stylesheet" type="text/css">
	<link href="{{ asset('assets/limitless/css/components.css') }}" rel="stylesheet" type="text/css">

	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script type="text/javascript" src="{{ asset('assets/limitless/js/core/libraries/jquery.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/limitless/js/core/libraries/bootstrap.min.js') }}"></script>
	<!-- /core JS files -->

	<!-- Theme JS files -->
	<script type="text/javascript" src="{{ asset('assets/limitless/js/core/app.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/limitless/js/core/layout.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/limitless/js/plugins/forms/styling/switch.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/limitless/js/plugins/ui/nicescroll.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/limitless/js/plugins/ui/ripple.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/limitless/js/plugins/tables/datatables/datatables.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/limitless/js/plugins/tables/datatables/extensions/fixed_header.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/js/dataTables.bootstrap.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/limitless/js/plugins/notifications/sweet_alert.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/limitless/js/plugins/notifications/jgrowl.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/limitless/js/plugins/visualization/c3/c3.min.js') }}"></script>

	<script type="text/javascript" src="{{ asset('assets/limitless/js/plugins/visualization/c3/c3_axis.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/limitless/js/plugins/visualization/c3/c3_advanced.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/limitless/js/plugins/visualization/c3/c3_bars_pies.js') }}"></script>
	<script type="text/javascript" src="{{ asset('assets/limitless/js/plugins/visualization/c3/c3_grid.js') }}"></script>


	<!-- <script type="text/javascript" src="{{ asset('assets/limitless/js/plugins/visualization/d3/d3_tooltip.js') }}"></script> -->
	<script type="text/javascript" src="{{ asset('assets/limitless/js/plugins/visualization/d3/d3.min.js') }}"></script>
	<!-- <script type="text/javascript" src="{{ asset('assets/limitless/js/plugins/visualization/d3/venn.js') }}"></script> -->


	<!-- Date Picker -->
	<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
	<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
	<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
	<!-- /Date Picker -->



	<!-- Data Table -->
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js"></script>

	<!-- Chart JS -->
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.js"></script>


	<!-- /theme JS files -->


	<script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>

	@stack('styles')
	@stack('scripts')
	<script>
	$(document).ready(function () {
		var scrollTo = parseInt($('li.active').eq(0).offset().top) - 50;
		// console.log(scrollTo);
		$(".sidebar-fixed .sidebar-content").getNiceScroll(0).doScrollTop(scrollTo, 0)
	});
	</script>

</head>
<style>
            body{
            font-size: 14px;
        }
</style>
<body class="navbar-top">
    <div  style="text-align:center;background-color:#C9EEFF">
        <br/>
        <h2>Supporting Web Application for Improving Human-Relaitionship</h2>
        <br/>

    </div>
    <nav class="navbar navbar-expand-sm navbar-light " style="color:#2699FB;border-bottom:solid #2699FB 1px ;height:45px">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <div class="container">
          <ul class="navbar-nav">
            <li class="nav-item active" style="border-left:solid #2699FB 2px">
              <a style="color:#2699FB;" class="nav-link" href="#">Menu <span class="sr-only">(current)</span></a>
            </li>



          </ul>
          <div style="float:right">
            <ul class="navbar-nav">

            <li class="nav-item dropdown" >
              <a style="color:#2699FB;"class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i style="font-size:20px"class="fa fa-user "></i> {{ Auth::user()->name}} {{ Auth::user()->lname}}
              </a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                <a class="dropdown-item" href="/logout">Logout</a>
              </div>
            </li>
        </ul>

        </div>
        </div>

        </div>
      </nav>
	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->

			<!-- Main content -->

				<!-- Page header -->

				<!-- /page header -->


				<!-- Content area -->

					@yield('content')

					<!-- Footer -->
                    @include('layouts.footer-per')
					<!-- /footer -->

				<!-- /content area -->

			<!-- /main content -->

		<!-- /page content -->

	</div>
	<!-- /page container -->

</body>
</html>
