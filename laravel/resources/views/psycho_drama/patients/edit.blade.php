
@extends('psycho_drama.base')
@section('action-content')
    <!-- Main content -->
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">

<style>
    body{
        font-size:14px;
    }
    .columnimg{
        float: left;
        width:25%;
        text-align:center
    }
    .margi{
        float: left;
        margin-top:40px;
        width:25%;

    }
    .columnselectbirth{
        float: left;
        width:30%;
        padding:2px
    }
    .column1{
        float: left;
        width:10%;
        padding:5px;
    }
    .column2{
        float: left;
        width:45%;
        padding:5px;

    }
    .columnbirthd{
        float: left;
        width:100%;
        padding:5px;
    }
    .columncode{
        float: left;
        width:50%;
        padding:5px;
    }
    .w3-border-blue{
        border-color:#BCE0FD;
    }
    .custom-file-input {
  color: transparent;
}
.custom-file-input::-webkit-file-upload-button {
  visibility: hidden;
}
.custom-file-input::before {
  content: 'Select some files';
  color: black;
  display: inline-block;
  background: -webkit-linear-gradient(top, #f9f9f9, #e3e3e3);
  border: 1px solid #999;
  border-radius: 3px;
  padding: 5px 8px;
  outline: none;
  white-space: nowrap;
  -webkit-user-select: none;
  cursor: pointer;
  text-shadow: 1px 1px #fff;
  font-weight: 700;
  font-size: 10pt;
}
.custom-file-input:hover::before {
  border-color: black;
}
.custom-file-input:active {
  outline: 0;
}
.custom-file-input:active::before {
  background: -webkit-linear-gradient(top, #e3e3e3, #f9f9f9);
}
</style>

    <section class="content">

        <section class="content">
            <div>

    </div>
    <br/>
  <div class="container">
  <div class="box-header">

  </div>
  <!-- /.box-header -->
  <div class="box-body">
      <div class="row">
        <div class="col-sm-6"></div>
        <div class="col-sm-6"></div>
      </div>

      <div  >

            <div class="container">

               <div style="border-left:solid #2196F3 3px"> <h5 style="padding:7px"> Edit {{$patient->name}} profile</h5></div>

               <div style="width:100%">
                <form class="form-horizontal" role="form" method="POST" action="{{ route('patient.update', ['id' => $patient->id]) }}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div style="float:left;width:20%;text-align:center" >
                    <div class="columnimg">
                        @if($patient->img !=NULL)
                        <img src="{{url('../')}}/patients/{{$patient->img}}" id="thumbnil" style="border-radius: 50%;border:solid silver 0.5px"  width="200px" height="200px">
                        @else
                        <img src="{{url('../')}}/patients/user.jpg" id="thumbnil" style="border-radius: 50%;border:solid silver 0.5px"  width="200px" height="200px">
                        @endif
                        <br/>
                        <br/>

                        <input type="file" name="img_file" onchange="showMyImage(this)"/>
                       	<!--  <button class="w3-btn w3-white w3-border w3-border-blue w3-round-large" style="margin-left:35px;border-color:blue">Upload picture</button>-->
                    </div>
                </div>
                <div style="float:right;width:80%">
                    <div class="column1">
                    <label for="fname">Title:</label><br>
                    <select class="form-control" name="title" required >
                        <option value="1"{{$patient->title == 1 ? 'selected' : ''}}>Mrs.</option>
                        <option value="2"{{$patient->title == 2 ? 'selected' : ''}}>Mr.</option>
                    </select>
                </div>
                    <div class="column2"><label for="fname">First name:</label><br>
                    <input type="text" class="form-control" id="name" name="name"  style="width:100%" value="{{$patient->name}}" required autofocus>
                    </div>

                    <div class="column2"><label for="fname">Last name:</label><br>
                        <input type="text"class="form-control" id="lname" name="lname"  style="width:100%" value="{{$patient->lname}}">
                    </div>
                        <div class="columnbirthd"><label for="fname">Birthday</label><br>
                            <div style="width:50%">
                                <div class="columnselectbirth">
                                <select class="form-control" name="daybirth">
                                    <option>Day</option>
                                    <option value ="01" {{$patient->daybirth == "01" ? 'selected' : ''}}>  01  </option>
                                    <option value ="02" {{$patient->daybirth == "02" ? 'selected' : ''}}>  02  </option>
                                    <option value ="03" {{$patient->daybirth == "03" ? 'selected' : ''}}>  03  </option>
                                    <option value ="04" {{$patient->daybirth == "04" ? 'selected' : ''}}>  04  </option>
                                    <option value ="05" {{$patient->daybirth == "05" ? 'selected' : ''}}>  05  </option>
                                    <option value ="06" {{$patient->daybirth == "06" ? 'selected' : ''}}>  06  </option>
                                    <option value ="07" {{$patient->daybirth == "07" ? 'selected' : ''}}>  07  </option>
                                    <option value ="08" {{$patient->daybirth == "08" ? 'selected' : ''}}>  08  </option>
                                    <option value ="09" {{$patient->daybirth == "09" ? 'selected' : ''}}>  09  </option>

                                  @for ($i =10; $i <= 31; $i++)
                                    <option value="{{ $i }}" {{$patient->daybirth == $i ? 'selected' : ''}}>{{ $i }}</option>
                                    @endfor
                                </select>
                            </div>
                            <div class="columnselectbirth">
                                <select class="form-control" name="monthbirth">
                                    <option value="">Month</option>
                                    <option value ="01" {{$patient->monthbirth == "01" ? 'selected' : ''}}>  January  </option>
                                    <option value ="02" {{$patient->monthbirth == "02" ? 'selected' : ''}}>  February  </option>
                                    <option value ="03" {{$patient->monthbirth == "03" ? 'selected' : ''}}>  March  </option>
                                    <option value ="04" {{$patient->monthbirth == "04" ? 'selected' : ''}}>  April   </option>
                                    <option value ="05" {{$patient->monthbirth == "05" ? 'selected' : ''}}>  May  </option>
                                    <option value ="06" {{$patient->monthbirth == "06" ? 'selected' : ''}}>  June  </option>
                                    <option value ="07" {{$patient->monthbirth == "07" ? 'selected' : ''}}>  July  </option>
                                    <option value ="08" {{$patient->monthbirth == "08" ? 'selected' : ''}}>  August  </option>
                                    <option value ="09" {{$patient->monthbirth == "09" ? 'selected' : ''}}>  September  </option>
                                    <option value ="10" {{$patient->monthbirth == "10" ? 'selected' : ''}}>  October  </option>
                                    <option value ="11" {{$patient->monthbirth == "11" ? 'selected' : ''}}>  November  </option>
                                    <option value ="12" {{$patient->monthbirth == "12" ? 'selected' : ''}}>  December  </option>
                                </select>
                            </div>
                            <div class="columnselectbirth">
                                <select class="form-control" name="yearbirth">
                                    <option>Year</option>
                                    @for ($i =$currentyear; $i >= 1900; $i--)

                                    <option value="{{ $i }}" {{$patient->yearbirth == $i ? 'selected' : ''}}>{{ $i }}</option>
                                    @endfor
                                </select>
                            </div>
                    </div>
                    </div>

                    <div class="columnbirthd"><label for="fname">Address</label><br>
                    <textarea type="text"class="form-control" name="address">{{$patient->address}}</textarea>
                    </div>
                    <div class="columncode"><label for="fname">Postal code:</label><br>
                        <input type="text"class="form-control" name="postal_code"  style="width:100%" value="{{$patient->postal_code}}">
                    </div>
                    <div class="columncode"><label for="fname">Prefectures:</label><br>
                        <input type="text"class="form-control" name="prefectures"  style="width:100%" value="{{$patient->prefectures}}">
                    </div>
                    <div style="width:100%">
                    <div class="columnbirthd"><label for="fname">Country:</label><br>
                        <input type="text"class="form-control" name="country" style="width:49.3%" value="{{$patient->country}}">
                    </div>
                    </div>
                    <div class="columncode"><label for="fname">Email:</label><br>
                        <input type="email"class="form-control" name="email" style="width:100%" value="{{$patient->email}}">
                    </div>
                    <div class="columncode"><label for="fname">Telephone:</label><br>
                        <input type="text" class="form-control"  name="tel"  style="width:100%" value="{{$patient->tel}}">
                    </div>

                    <div style="float:right;margin-top:15px" >
                    <a class="w3-btn w3-white w3-border w3-border-blue  w3-round-large " href="/allpatient">Back</a>
                    <button type="submit" class="w3-btn w3-blue  w3-round-large" >Update</button>
                    </div>
                </div>

                </form>
                </div>



              </form>
        </div>
  </div>
  <!-- /.box-body -->

</div>
    </section>

    </section>
    @endsection
    @push('scripts')

    <script type="text/javascript">
        $(document).ready(function() {

            exportFile('#datatable', 'psychodrama_patients');

            function exportFile(element, file_name) {

                $(element).DataTable( {
                    dom: 'Bfrtip',
                    "paging":true,

                    "columnDefs": [
                    { "orderable": false, "targets": 1 },
                    { "orderable": false, "targets": 3 },
                    { "orderable": false, "targets": 7 }

                    ],
                    buttons: [

                        {
                            text: 'Add Report',
                            action: function ( e, dt, button, config ) {
                            window.location = '/creatreport';
                        }
                        },
                                              /* {
                            extend: 'copyHtml5',
                            bom: true,
                            title: file_name
                        },
                        {
                            extend: 'csvHtml5',
                            bom: true,
                            title: file_name
                        },
                        {
                            extend: 'excelHtml5',
                            bom: true,
                            title: file_name
                        },
                        {
                            extend: 'pdfHtml5',
                            bom: true,
                            title: file_name,
                        },*/

                    ],

                });
            }

        });

    </script>
<script>
    function showMyImage(fileInput) {
           var files = fileInput.files;
           for (var i = 0; i < files.length; i++) {
               var file = files[i];
               var imageType = /image.*/;
               if (!file.type.match(imageType)) {
                   continue;
               }
               var img=document.getElementById("thumbnil");
               img.file = file;
               var reader = new FileReader();
               reader.onload = (function(aImg) {
                   return function(e) {
                       aImg.src = e.target.result;
                   };
               })(img);
               reader.readAsDataURL(file);
           }
       }
    </script>


    @endpush
