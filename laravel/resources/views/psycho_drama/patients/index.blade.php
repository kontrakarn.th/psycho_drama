
@extends('psycho_drama.base')
@section('action-content')
    <!-- Main content -->
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">

    <style>
            body{
        font-size:14px;
    }
        .button5 {border-radius: 50%;
                  border-color:white}

        #datatable {
            border-collapse: collapse;
            width: 100%;

        }

        #datatable td, #datatable th {
            border: 1px solid #ddd;
            padding: 8px;
            border:none;

        }

#datatable tr:nth-child(even){background-color: #EFFAFF;
}

/*#customers tr:hover {background-color: #ddd;}*/

#datatable th {
    border:none;

  padding-top: 12px;
  padding-bottom: 12px;
  text-align: left;
  background-color: #C9EEFF;

}
.myDIV:hover{
    cursor: pointer
}
/*.dataTables_filter {
   display: none;
}*/
.dataTables_filter input {
  border-bottom: solid silver 0.5px;
}
    </style>

    <section class="content">

        <section class="content">
            <div>

    </div>
    <br/>
  <div class="container">


    <div >
      <div  >
            <div class="container">
                <div style="border-left:solid #2196F3 3px"> <h5 style="padding:7px">Patient</h5></div>
                <div style="overflow-x:auto;width:1200px" >
                    <table style="" id="datatable"  data-page-length="10" width="1100px">
                    <thead style="background-color:#C9EEFF;">
                        <tr>
                            <th style="display:none"></th>
                            <th class="no-sort" width="10" ></th>
                          <th scope="col" width="100">No.</th>
                          <th scope="col" width="15"></th>
                          <th scope="col">Patient Name</th>
                          <th scope="col">Email</th>
                          <th scope="col">Created at</th>
                          <th class="no-sort"  ></th>
                          <th class="myDIV"  scope="col" style="background-color:#FFA5A5;text-align:center ;" width="85"><a class=" delete_all" data-url="{{ url('myproductsDeleteAll') }}"><i style="font-size:18px" class="fa fa-trash"></i> DELETE</a></th>

                        </tr>
                      </thead>
                      <tbody>
                  @foreach($report as $index =>$re)

                    <tr id="tr_{{$re->id}}">
                    <td style="display:none"></td>
                    <td><input type="checkbox" class="sub_chk" data-id="{{$re->id}}"></td>
                      <td>{{++$index}}</td>
                      <td><img src="{{url('../')}}/patients/{{$re->img}}"  style="border-radius: 50%;"  width="50" height="50"></td>
                      <td>
                        {{$re->name}} {{$re->lname}}
                      </td>
                      <td><a href = "mailto:{{$re->email}}">{{$re->email}}</a></td>
                      <td>{{$re->date_create}} {{$re->time_create}}</td>
                      <td class="myDIV" style="background-color:#EAEAEA;text-align:center;border:solid white"><a href="/createreport?patient_id={{$re->id}}"><span style="font-size:13px" >SELECT</span> </a></td>
                      <td class="myDIV" style="background-color:#EAEAEA;text-align:center;border:solid white"><a href="/edit_patient/{{$re->id}}"><i style="font-size:24px" class="fa fa-edit"></i> </a></td>

                    </tr>
                    @endforeach
                </tbody>
                  </table>
                </div>

                <a  style="float:left" class="w3-btn w3-white w3-border w3-border-blue  w3-round-large " href="/home">Back</a>



        </div>
    </div>
  </div>
  <!-- /.box-body -->

</div>
    </section>

    </section>
    @endsection
    @push('scripts')

    <script type="text/javascript">
        $(document).ready(function() {

            exportFile('#datatable', 'psychodrama_patients');

            function exportFile(element, file_name) {

                $(element).DataTable( {
                    dom: 'Bfrtip',
                    "paging":true,

                    "columnDefs": [
                    { "orderable": false, "targets": 1 },
                    { "orderable": false, "targets": 3 },
                    { "orderable": false, "targets": 7 }
                    { "orderable": false, "targets": 8 }

                    ],
                    buttons: [

                        {
                            text: 'Add patient',
                            className:'w3-btn w3-blue  w3-round-large',
                            action: function ( e, dt, button, config ) {
                            window.location = '/create_patients';
                        }
                        },
                                              /* {
                            extend: 'copyHtml5',
                            bom: true,
                            title: file_name
                        },
                        {
                            extend: 'csvHtml5',
                            bom: true,
                            title: file_name
                        },*/
                        {
                            extend: 'excelHtml5',
                            className:'w3-btn w3-blue  w3-round-large',

                            bom: true,
                            title: file_name
                        },
                        {
                            extend: 'pdfHtml5',
                            className:'w3-btn w3-blue  w3-round-large',

                            bom: true,
                            title: file_name,
                        },

                    ],

                });
            }

        });

    </script>
    <script type="text/javascript">
        $(document).ready(function () {


            $('#master').on('click', function(e) {
             if($(this).is(':checked',true))
             {
                $(".sub_chk").prop('checked', true);
             } else {
                $(".sub_chk").prop('checked',false);
             }
            });


            $('.delete_all').on('click', function(e) {


                var allVals = [];
                $(".sub_chk:checked").each(function() {
                    allVals.push($(this).attr('data-id'));
                });


                if(allVals.length <=0)
                {
                    alert("Please select row.");
                }  else {


                    var check = confirm("Are you sure you want to delete this row?");
                    if(check == true){


                        var join_selected_values = allVals.join(",");


                        $.ajax({
                            url: $(this).data('url'),
                            type: 'DELETE',
                            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                            data: 'ids='+join_selected_values,
                            success: function (data) {
                                if (data['success']) {
                                    $(".sub_chk:checked").each(function() {
                                        $(this).parents("tr").remove();
                                    });
                                    alert(data['success']);
                                    location.href = "/allpatient";

                                } else if (data['error']) {
                                    alert(data['error']);
                                } else {
                                    alert('Whoops Something went wrong!!');
                                }
                            },
                            error: function (data) {
                                alert(data.responseText);
                            }
                        });


                      $.each(allVals, function( index, value ) {
                          $('table tr').filter("[data-row-id='" + value + "']").remove();
                      });
                    }
                }
            });


            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]',
                onConfirm: function (event, element) {
                    element.trigger('confirm');
                }
            });


            $(document).on('confirm', function (e) {
                var ele = e.target;
                e.preventDefault();


                $.ajax({
                    url: ele.href,
                    type: 'DELETE',
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    success: function (data) {
                        if (data['success']) {
                            $("#" + data['tr']).slideUp("slow");
                            alert(data['success']);
                        } else if (data['error']) {
                            alert(data['error']);
                        } else {
                            alert('Whoops Something went wrong!!');
                        }
                    },
                    error: function (data) {
                        alert(data.responseText);
                    }
                });


                return false;
            });


        });

    </script>

    @endpush
