@extends('layouts.master')
@section('content')
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->

    @yield('action-content')
    <!-- /.content -->
  </div>
@endsection
