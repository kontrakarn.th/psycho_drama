import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import LineTo from 'react-lineto';
import Draggable from 'react-draggable'; // The default;
import ResizableRect from 'react-resizable-rotatable-draggable';
import ClickNHold from 'react-click-n-hold';

export default class Testrotate extends Component {
  constructor(){
    super();
    //console.log(super());
    this.state = {
      width: 100,
      height: 100,
      top: 100,
      left: 100,
      rotateAngle: 0,
      ro:0,
      ro2:0,
      width:170,
      height:150,
      width2:170,
      height2:150,
      fromchr:'',
      tochr:'',
      borderclr:'',
      activeDrags: 0,
      selectedcharactor1:"0",
      selectedforrelation :[],
      relation :[],
      stage:1,
      movepositionid:'',
      charactorname:'',
      note:'',
      title:'',

      deltaPosition: {
        x: 0, y: 0
      },
      arr:[]
    }
    this.handleResize = this.handleResize.bind(this);
    this.handleRotate = this.handleRotate.bind(this);
    this.rotate = this.rotate.bind(this);
    this.rela = this.rela.bind(this);
    this.rela2 = this.rela2.bind(this);
    this.positiverel = this.positiverel.bind(this);
    this.negativerel = this.negativerel.bind(this);
    this.addMch = this.addMch.bind(this);
    this.addWch = this.addWch.bind(this);
    this.delCH = this.delCH.bind(this);
    this.inputcharactorname = this.inputcharactorname.bind(this);
    this.Changenote = this.Changenote.bind(this);
    this.Changetitle = this.Changetitle.bind(this);

    this.onStart = this.onStart.bind(this);
    this.onStop = this.onStop.bind(this);
    this.clickStart = this.clickStart.bind(this);
    this.clickStop = this.clickStop.bind(this);
    this.handleDrag = this.handleDrag.bind(this);
    this.showadd = this.showadd.bind(this);
    this.clickNHold = this.clickNHold.bind(this);
    this.delrela = this.delrela.bind(this);
    this.ClearselectedCharactor = this.ClearselectedCharactor.bind(this);
    this.changeStage = this.changeStage.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);

  }
  componentDidMount() {
    axios.post('/create/charactor/',{
        reportid:this.props.reportid,
      }).then(res=>{
        this.setState({
            arr : res.data,
        });
      });
  }

  handleResize  (style, isShiftKey, type)  {
    // type is a string and it shows which resize-handler you clicked
    // e.g. if you clicked top-right handler, then type is 'tr'
    let { top, left, width, height } = style
    top = Math.round(top)
    left = Math.round(left)
    width = Math.round(width)
    height = Math.round(height)
    this.setState({
      top,
      left,
      width,
      height,
      ro:0

    })
  }

  handleRotate  (rotateAngle) {
    this.setState({
      rotateAngle
    })
  }

  handleDrag (e) {
    console.log(e)
  }
  rotate(){
      if(this.state.selectedcharactor1 == null ||this.state.selectedcharactor1 == 0){
          alert("No Charactor Selected")
      }else{

    axios.post('/update/charactor/',{
        ch_id:this.state.selectedcharactor1,
      }).then(res=>{
        this.setState({
            arr : res.data,
        });
      });

    }
  }

  eventLogger  (e: MouseEvent, data: Object)  {
    console.log('Event: ', e);
    console.log('Data: ', data);
  };
  rela(e){
        this.setState({selectedcharactor1:e.id})
        var n = this.state.selectedforrelation.includes(e.id);
        if(n == true){

        }else{
          if(this.state.selectedforrelation.length >=2){
            alert("Can't Select charactor more than 2")
            this.state.selectedforrelation.length= 0
          }else{
            this.state.selectedforrelation.push(e.id)
          }
        }

    console.log(this.state.selectedforrelation)
    console.log(this.state.selectedforrelation.length)


  }
  rela2(){
    this.setState({
      width2:Number(this.state.width2)+120,
      height2:Number(this.state.height2)+100

    })
  }
  ClearselectedCharactor(){
    this.setState({
        selectedforrelation:[],
        selectedcharactor1:0
    });
  }
  positiverel(){

    axios.post('/create/relation/',{
        reportid:this.props.reportid,
        charactor:this.state.selectedforrelation,
        type:1,
      }).then(res=>{
          console.log(res.data)
        this.setState({
            relation : res.data,
            selectedforrelation:[]

        });
      });

  }
  negativerel(){

    axios.post('/create/relation/',{
        reportid:this.props.reportid,
        charactor:this.state.selectedforrelation,
        type:2,
      }).then(res=>{
        this.setState({
            selectedforrelation:[],
            relation : res.data,
        });
      });
  }
  delrela(){
    axios.post('/delete/relation/',{
      reportid:this.props.reportid,
      charactor:this.state.selectedforrelation,
      type:2,
    }).then(res=>{
      this.setState({
          relation : res.data,
          selectedforrelation:[]

      });
    });
  }
  addMch(){
    axios.post('/create/charactor/',{
        reportid:this.props.reportid,
      }).then(res=>{
        this.setState({
            arr : res.data,
        });
      });

  }
  delCH(){
    axios.post('/delete/charactor/',{
        reportid:this.props.reportid,
        charactorid:this.state.selectedcharactor1,
      }).then(res=>{
        this.setState({
            arr : res.data,
        });
      });

  }
  addWch(){
     this.state.arr.push({
                            "id":3,
                            "ch_type_id":2,
                            "user_id":1,
                            "rep_id":1,
                            "ch_name":1,
                            "ch_width":120,
                            "ch_hieght":150,
                            "ch_rotate":1,
                            "ch_position":1,
                            "user_ch":1,
    })
    this.setState({
        arr:this.state.arr,

      })
  }
  inputcharactorname(e){
      console.log(e.target.value)
    axios.post('/update/charactor/',{
        type:'name',
        ch_id:this.state.movepositionid,
        name:e.target.value
      }).then(res=>{
        this.setState({
            arr : res.data,
        });
      });
  }
  Imageshow(data){
    let styles = {
        transform: `rotate(${data.ch_rotate}deg)`,

    };
    if(data.ch_type_id == 1){
        if(data.user_ch == 1){
            return  <div style={{textAlign:'center'}}><img   className={data.class_name} width={data.ch_width} height={data.ch_height} style={styles} src="http://localhost:8000/img/you.png"/><br/><b>{this.props.patientname}</b></div>

        }else{
            return  <div><img   className={data.class_name} width={data.ch_width} height={data.ch_height} style={styles} src="http://localhost:8000/img/people.png"/></div>

        }
    }
    else{
        if(data.user_ch == 1){
            var n = this.state.selectedforrelation.includes(data.id);

            if(n == true){
                return  <div style={{textAlign:'center'}}><img   className={data.class_name} width={data.ch_width} height={data.ch_height} style={styles} src="http://localhost:8000/img/you_shadow.png"/><br/><b style={{marginLeft:'12px',fontSize:'12px'}}>{this.props.patientname}</b></div>
            }else{
                return  <div style={{textAlign:'center'}}><img   className={data.class_name} width={data.ch_width} height={data.ch_height} style={styles} src="http://localhost:8000/img/you.png"/><br/><b style={{marginLeft:'12px',fontSize:'12px'}}>{this.props.patientname}</b></div>
            }


        }else{
            var n = this.state.selectedforrelation.includes(data.id);
            if(n == true){
                var name = data.name
                return  <div><img   className={data.class_name} width={data.ch_width} height={data.ch_height} style={styles} src="http://localhost:8000/img/people_shadow.png"/> <br/> <input onChange={this.inputcharactorname} value={name} size="10" style={{textAlign:'center',fontSize:'12px',marginTop:'8px'}} autoFocus/></div>
            }else{
                return  <div><img   className={data.class_name} width={data.ch_width} height={data.ch_height} style={styles} src="http://localhost:8000/img/people.png"/> <br/>  <input onChange={this.inputcharactorname} value={name} size="10" style={{textAlign:'center',fontSize:'12px',marginTop:'8px'}} autoFocus/></div>
            }

        }
    }
  }
  onStart  (data) {
      console.log('start',data.id)
    this.setState({activeDrags: ++this.state.activeDrags,
        deltaPosition:{
            x: data.ch_position_x, y: data.ch_position_y
        }});
  };

  onStop (data){
   // console.log('movepositionid',this.state.movepositionid);
   axios.post('/update/charactor/',{
    type:'move',
    ch_id:data.id,
    position_x:this.state.deltaPosition.x,
    position_y:this.state.deltaPosition.y,
  }).then(res=>{
    this.setState({
        arr : res.data,
        activeDrags: --this.state.activeDrags
    });
  });
    this.setState({activeDrags: --this.state.activeDrags});
  };
  handleDrag (e, ui){
    const {x, y} = this.state.deltaPosition;

    this.setState({
      deltaPosition: {
        x: x + ui.deltaX,
        y: y + ui.deltaY,
      }
    });
  };
  showadd(){
    console.log("Yess")
  }
  clickStart(e){
    console.log('START');
}

    clickStop(e, enough){
    console.log('END');
    console.log(enough ? 'Click released after enough time': 'Click released too soon');
}

	clickNHold(e){
		console.log('CLICK AND HOLD');
    }
    linerelation(data){
        if(data.rel_type_id == 1){
          return  <LineTo borderWidth="5px" from={data.rel_from_ch_class_name} to={data.rel_to_ch_class_name} borderColor="blue"/>
        }else if (data.rel_type_id == 2){
            return  <LineTo borderWidth="5px" from={data.rel_from_ch_class_name} to={data.rel_to_ch_class_name} borderColor="red"/>
        }
    }

    Linefunction(){
            //if(this.state.selectedforrelation.length < 2){
                            if(this.state.selectedforrelation.length < 2){
                return <div class="card" style={{marginTop:'10px'}}>
                <div class="card-header" style={{backgroundColor:'#2196F3',color:'white',textAlign:'center'}}>
                    Association line
                </div>
                    <div class="card-body">
                        <div  style={{textAlign:'center'}}>
                        <div style={{borderBottom:'solid silver 0.5px'}}>
                                <button disabled class="btn"><img   className="rotate"  style={{width:'35px'}} src="http://localhost:8000/img/unhappy_disabled.png"/></button><br/><span style={{color:'#2141F3',fontSize:'12px'}}>ADD NEGATIVE LINE</span>
                            </div>
                            <div style={{borderBottom:'solid silver 0.5px'}}>

                            <button  disabled class="btn"><img   className="rotate"  style={{width:'35px'}} src="http://localhost:8000/img/happyline_disabled.png"/></button><br/><span style={{color:'#E22020',fontSize:'12px'}}>ADD POSITIVE LINE</span>
                            </div>
                            <div style={{marginTop:'10px'}}>
                            <button disabled  class="btn"><img   className="rotate"  style={{width:'35px'}} src="http://localhost:8000/img/del_button_disabled.png"/></button><br/><span style={{fontSize:'12px'}}>DELETE LINE</span>
                            </div>
                        </div>

                      </div>
                    </div>
            }else{
                return <div class="card" style={{marginTop:'10px'}}>
            <div class="card-header" style={{backgroundColor:'#2196F3',color:'white',textAlign:'center'}}>
                Association line
            </div>
                <div class="card-body">
                    <div  style={{textAlign:'center'}}>
                    <div style={{borderBottom:'solid silver 0.5px'}}>

                            <button onClick={this.positiverel} class="btn"><img   className="rotate"  style={{width:'35px'}} src="http://localhost:8000/img/unhappy.png"/></button><br/><span style={{color:'#2141F3',fontSize:'12px'}}>ADD NEGATIVE LINE</span>
                        </div>
                        <div style={{borderBottom:'solid silver 0.5px'}}>

                        <button  onClick={this.negativerel} class="btn"><img   className="rotate"  style={{width:'35px'}} src="http://localhost:8000/img/happyline.png"/></button><br/><span style={{color:'#E22020',fontSize:'12px'}}>ADD POSITIVE LINE</span>
                        </div>
                        <div style={{marginTop:'10px'}}>
                        <button  onClick={this.delrela} class="btn"><img   className="rotate"  style={{width:'35px'}} src="http://localhost:8000/img/del_button.png"/></button><br/><span style={{fontSize:'12px'}}>DELETE LINE</span>
                        </div>
                    </div>

                  </div>
                </div>
            }



    }

    Rotation(){

        if(this.state.selectedcharactor1 < 1){
            return             <div class="card">
            <div class="card-header" style={{backgroundColor:'#2196F3',color:'white',textAlign:'center'}}>
                Icon
            </div>
                <div class="card-body">
                    <div style={{textAlign:'center'}}> <img   className="rotate"  style={{width:'70px'}} src="http://localhost:8000/img/peopleadd.png"/></div>
                    <div  style={{borderBottom:'solid silver 1px',height:'55px'}}>
                        <div style={{float:'left'}}>
                            <button onClick={this.addMch} class="btn"><img   className="rotate"  style={{width:'35px'}} src="http://localhost:8000/img/add_button.png"/></button>
                        </div>
                        <div style={{float:'right'}}>
                        <button onClick={this.delCH} class="btn"><img   className="rotate"  style={{width:'35px'}} src="http://localhost:8000/img/del_button.png"/></button>
                        </div>
                    </div>

                    <div style={{width:"100%",textAlign:'center',marginTop:'10px'}}>

<button  class="btn" ><img   className="rotate"  style={{width:'40px'}} src="http://localhost:8000/img/rotate_disabled.png"/></button>
</div>
                  </div>
                  <div style={{width:"100%",textAlign:'center',marginTop:'10px'}}>

                        <button style={{fontSize:'11px',display:'none'}} onClick={this.ClearselectedCharactor}  >Clear</button>
                        </div>
                </div>
        }else{
            return <div class="card">
            <div class="card-header" style={{backgroundColor:'#2196F3',color:'white',textAlign:'center'}}>
                Icon
            </div>
                <div class="card-body">
                    <div style={{textAlign:'center'}}> <img   className="rotate"  style={{width:'70px'}} src="http://localhost:8000/img/peopleadd.png"/></div>
                    <div  style={{borderBottom:'solid silver 1px',height:'55px'}}>
                        <div style={{float:'left'}}>
                            <button onClick={this.addMch} class="btn"><img   className="rotate"  style={{width:'35px'}} src="http://localhost:8000/img/add_button.png"/></button>
                        </div>
                        <div style={{float:'right'}}>
                        <button onClick={this.delCH} class="btn"><img   className="rotate"  style={{width:'35px'}} src="http://localhost:8000/img/del_button.png"/></button>

                        </div>


                    </div>

                    <div style={{width:"100%",textAlign:'center',marginTop:'10px'}}>

<button onClick={this.rotate} class="btn" ><img   className="rotate"  style={{width:'40px'}} src="http://localhost:8000/img/rotate.png"/></button>
</div>
<div style={{textAlign:'center'}}>

<button style={{fontSize:'11px',display:'none'}} onClick={this.ClearselectedCharactor}  >Clear</button>
</div>
                  </div>
                </div>

        }
    }
    showActionBar(){
        if(this.state.stage == 1){
            return <div>{this.Rotation()}</div>
        }else if(this.state.stage == 2){
            return <div>{this.Linefunction()}</div>
        }

    }
    changeStage(){
        if(this.state.stage == 1){
            this.setState({
                stage:2
              });
        }else if(this.state.stage == 2)
        this.setState({
                stage:3
          });
    }
    Clickchar(data){
        console.log(data.id);
    }

    clickordoubleclick(data){

            return <div  onDoubleClick={e => this.rela(data)} >
            {this.Imageshow(data)}
            </div>

    }
    handleSubmit(e){
        e.preventDefault();
        axios.post('/updatereport',{
            reportid:this.props.reportid,
            title:this.state.title,
            comment:this.state.note,

        }).then(res=>{
            window.location.href = "allpatient";
        });
      }
      Changenote(e){
        this.setState({
            note:e.target.value,
          })
      }
      Changetitle(e){
        this.setState({
            title:e.target.value,
          })
      }
    showFunction(){
        if(this.state.stage == 3){
            return <div>
                <form onSubmit={this.handleSubmit}>
            <button type="submit" onClick={this.changeStage} class="w3-btn w3-blue  w3-round-large" style={{float:'right'}}>Finish</button>
                                  <br/>
                                  <br/>
                                  <br/>



                    <input style={{border:'solid silver',width:'1100px',float:'right',overflow:'auto'}} placeholder="Title" value={this.state.title} onChange={this.Changetitle} required/><br/><br/><br/>
                    <textarea style={{border:'solid silver',width:'1100px',height:'200px',float:'right',overflow:'auto'}} placeholder="Note" onChange={this.Changenote} required>{this.state.note}</textarea>
                    </form>


                    <br/> <br/>
                    </div>
        }
        return <div>
<button type="submit" onClick={this.changeStage} class="w3-btn w3-blue  w3-round-large" style={{float:'right'}}>Next ></button>
                      <br/>
                      <br/>
                      <br/>

                    {this.state.relation.map(data =>
                  <LineTo fromAnchor="center"  toAnchor="center" borderWidth="5px" from={data.rel_from_ch_class_name} to={data.rel_to_ch_class_name} borderColor={data.color_style}/>


      )

      }
        <div style={{width:'200px',height:'450px',float:'left',borderRadius: '25px'}} className="container">
      {this.showActionBar()}
        </div>
        <div style={{border:'solid silver',width:'900px',height:'500px',float:'right',borderRadius: '25px',overflow:'auto'}} className="container">
            {this.state.arr.map(data =>
                <div  style={{width:'10px'}} width="10px" >
                 <Draggable
                    //axis="x"
                    //handle=".handle"
                    //position={{x: 0, y: 0}}
                    defaultPosition={{x: data.ch_position_x, y: data.ch_position_y}}
                    //grid={[25, 25]}
                    scale={1}
                    onDrag={this.handleDrag}
                   // onStart={e => this.onStart(data)}
                   // onStop={e => this.onStop(data)}
                    >
                    <div>
                        {this.clickordoubleclick(data)}
                     </div>
                </Draggable>

                </div>
      )}
      <br/>
        </div>
        <br/> <br/>
        </div>
    }
    render() {
        const dragHandlers = {onStart: this.onStart, onStop: this.onStop};

      const {width, top, left, height, rotateAngle} = this.state
      const x = 500;
      const y = 100;
      const {deltaPosition} = this.state;

     /* const styles = {
          transform: `translate(${x}px, ${y}px)`
      };*/

      const styles = {
          transform: `rotate(${this.state.ro}deg)`,

      };
      const styles2 = {
        transform: `rotate(${this.state.ro2}deg)`,

    };

      return (
          <div >
                    {this.showFunction()}


        </div>
      )
    }
  }

if (document.getElementById('testrotate')) {
    const component = document.getElementById('testrotate');
    const props = Object.assign({}, component.dataset);
      ReactDOM.render(<Testrotate {...props}/>, component);}
