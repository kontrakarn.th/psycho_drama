<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Relation extends Model
{
    //
    protected $table = 'relation';
    protected $guarded = [];

    public function Charactorfrom()
    {
        return $this->belongsTo('App\Charactor','rel_from_ch_class_name','class_name');
    }
    public function Charactorto()
    {
        return $this->belongsTo('App\Charactor','rel_to_ch_class_name','class_name');
    }
}
