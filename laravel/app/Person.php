<?php

namespace App;
use App\Person;
use App\User;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\PersonResetPasswordNotification;
class Person extends Authenticatable
{
    use Notifiable;
    protected $guard ='person';
    /**
    * The attributes that aren't mass assignable.
    *
    * @var array
    */
    protected $fillable = [
      'name','email','password',];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $table = 'persons';
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function sendPasswordResetNotification($token)
  {
      $this->notify(new PersonResetPasswordNotification($token));
  }

}
