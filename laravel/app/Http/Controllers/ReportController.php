<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Structure;
use Illuminate\Support\Facades\Auth;
use App\ReportType;
use App\Report;
use App\CharatorType;
use App\Charactor;
use App\RelationType;
use App\Relation;
use App\Patient;

use Session;

class ReportController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth:person');
    }
   public function create(Request $request){
    //$report =  Report::all();
    if($request->patient_id == NULL){
        return redirect()->back();
    }
    Report::where('created_by',Auth::user()->id)->where('patient_id',$request->patient_id)->delete();
    $savereport = New Report;
    $savereport->created_by = Auth::user()->id;
    $savereport->patient_id = $request->patient_id;
    $savereport->save();
    $reportid  = $savereport->id;
    $patientid  = $savereport->patient_id;
    $find = Patient::find($savereport->patient_id);
    $patientid  = $savereport->patient_id;
    $patientname  = $find->name;
    return view('psycho_drama/testrotate',compact('reportid','patientid','patientname'));
   }
   public function update(Request $request){
     $request->reportid;
    $report = Report::where('id',$request->reportid)->first();
    $report->rep_title = $request->title;
    $report->rep_comment = $request->comment;
    $report->save();

   }
  }
