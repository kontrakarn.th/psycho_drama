<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Structure;
use Illuminate\Support\Facades\Auth;
use App\ReportType;
use App\Report;
use App\CharatorType;
use App\Charactor;
use App\RelationType;
use App\Relation;

use Session;

class RelationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth:person');
    }
   public function store(Request $request){
    $selection1 = "ch".$request->charactor[0];
    $selection2 = "ch".$request->charactor[1];
    $findrelation = Relation::whereIn('rel_from_ch_class_name',[$selection1,$selection2])->whereIn('rel_to_ch_class_name',[$selection1,$selection2])->where('rep_id',$request->reportid)->delete();
    $relation = New Relation;
    $relation->rel_type_id = $request->type;
    $relation->rel_from_ch_class_name = "ch".$request->charactor[0];
    $relation->rel_to_ch_class_name = "ch".$request->charactor[1];
    if($request->type == 1){
        $relation->color_style = "blue";
    }else if ($request->type == 2){
        $relation->color_style = "red";
    }
    $relation->rep_id = $request->reportid;
    $relation->save();
    $relation = Relation::where('rep_id',$request->reportid)->get();
    return $relation;
   }
   public function delete(Request $request){
    $selection1 = "ch".$request->charactor[0];
    $selection2 = "ch".$request->charactor[1];
    $findrelation = Relation::whereIn('rel_from_ch_class_name',[$selection1,$selection2])->whereIn('rel_to_ch_class_name',[$selection1,$selection2])->where('rep_id',$request->reportid)->delete();
    $relation = Relation::where('rep_id',$request->reportid)->get();
    return $relation;
   }

   public function update(Request $request){
    $charactor = Charactor::find($request->ch_id);
    $charactor->ch_rotate =$charactor->ch_rotate+20 ;
    $charactor->save();
    $allcharactor = Charactor::where('rep_id',$charactor->rep_id)->get();
    return $allcharactor;
   }
  }
