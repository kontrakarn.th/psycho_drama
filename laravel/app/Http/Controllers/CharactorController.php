<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Structure;
use Illuminate\Support\Facades\Auth;
use App\ReportType;
use App\Report;
use App\CharatorType;
use App\Charactor;
use App\RelationType;
use App\Relation;

use Session;

class CharactorController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth:person');
    }
   public function store(Request $request){
    $allcharactor = Charactor::where('rep_id',$request->reportid)->get();

    $charactor = New Charactor;
    $charactor->ch_type_id = NULL;
    $charactor->user_id = NULL;
    $charactor->rep_id = $request->reportid;
    $charactor->ch_name = NULL;
    $charactor->ch_width = 70;
    $charactor->ch_height = 50;
    $charactor->ch_rotate = 1;
    $charactor->ch_position_x = $request->ch_position_x;
    $charactor->ch_position_y = $request->ch_position_y;

    if(count($allcharactor) < 1){
        $charactor->user_ch = 1;
        $charactor->ch_position_x = 400;
        $charactor->ch_position_y = 200;
    }else{
        $charactor->user_ch = NULL;
    }
    $charactor->save();
    $charactor = Charactor::find($charactor->id);
    $charactor->class_name = "ch".$charactor->id;
    $charactor->save();
    $allcharactor = Charactor::where('rep_id',$request->reportid)->get();
    return $allcharactor;
   }


   public function delete(Request $request){
    /*$findyou = Charactor::find($request->charactorid);
    if($findyou->user_ch == 1){

    }else{
        Relation::where('rel_from_ch_class_name',$request->charactorid)->Orwhere('rel_to_ch_class_name',$request->charactorid)->delete();
        Charactor::where('id',$request->charactorid)->delete();
    }*/

    $charactor = Charactor::Orderby('id','DESC')->first();
    if($charactor->user_ch == 1){

    }else{
        Relation::where('rel_from_ch_class_name',$charactor->id)->Orwhere('rel_to_ch_class_name',$charactor->id)->delete();
        Charactor::where('id',$charactor->id)->delete();
    }

    $allcharactor = Charactor::where('rep_id',$request->reportid)->get();
    return $allcharactor;
   }

   public function update(Request $request){
    if($request->type == "name"){
        $charactor = Charactor::find($request->ch_id);
        $charactor->ch_name =$request->name;
        $charactor->save();
        $allcharactor = Charactor::where('rep_id',$charactor->rep_id)->get();
        return $allcharactor;
       }
       if($request->type == "move"){
        $charactor = Charactor::find($request->ch_id);
        $charactor->ch_position_x =$request->position_x ;
        $charactor->ch_position_y =$request->position_y ;
        $charactor->save();
        $allcharactor = Charactor::where('rep_id',$charactor->rep_id)->get();
        return $allcharactor;
       }
    $charactor = Charactor::find($request->ch_id);
    $charactor->ch_rotate =$charactor->ch_rotate-20 ;
    $charactor->save();
    $allcharactor = Charactor::where('rep_id',$charactor->rep_id)->get();
    return $allcharactor;
   }
  }
