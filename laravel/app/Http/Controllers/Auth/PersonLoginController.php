<?php

namespace App\Http\Controllers\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\SidebarperController;
use Illuminate\Support\Facades\Redirect;

use Auth;
use Session;
use AuthenticatesUsers;

class PersonLoginController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }
    public function showLoginForm()
    {
      return view('auth.person-login');
    }

    public function login(Request $request)
    {

       $this->validate($request,[
         'email' => 'required|email',
         'password' => 'required|min:6'
       ]);
       if(Auth::guard('web')->attempt(['email' => $request->email,'password' => $request->password],$request->remember)){
        return redirect('/home');

       }
       return redirect()->back()->withInput($request->only('email','remember'));

    }

    public function logout()
  {
      Auth::guard('web')->logout();
      return redirect('/');
  }
}
