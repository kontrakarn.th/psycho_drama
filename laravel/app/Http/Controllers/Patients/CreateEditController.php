<?php

namespace App\Http\Controllers\Patients;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Structure;
use Illuminate\Support\Facades\Auth;
use App\ReportType;
use App\Report;
use App\CharatorType;
use App\Charactor;
use App\RelationType;
use App\Relation;
use App\Patient;
use App\Http\Controllers\Controller;
use Session;
use Image;
use Storage;
use File;

class CreateEditController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth:person');
        $this->middleware('auth');
    }
   public function create(Request $request){
    $redirectlink = $request->redirect;
    $currentyear = Date("Y");
     return view('psycho_drama/patients/create',compact('currentyear','redirectlink'));
   }

   public function store(Request $request){
    return $this->save($request);
   }
   private function save($request){
    date_default_timezone_set("Asia/Tokyo");
    $date = date("d M Y");
    $time = date("h:i:sa");

    $patient = New Patient;
    $patient->title = $request->title;
    $patient->name = $request->name;
    $patient->lname = $request->lname;
    $patient->daybirth = $request->daybirth;
    $patient->monthbirth = $request->monthbirth;
    $patient->yearbirth = $request->yearbirth;
    $patient->address= $request->address;
    $patient->postal_code = $request->postal_code;
    $patient->prefectures = $request->prefectures;
    $patient->country = $request->country;
    $patient->email = $request->email;
    $patient->tel = $request->tel;
    $patient->date_create = $date;
    $patient->time_create = $time;
    $patient->created_by = Auth::user()->id;

    if($request->file('img_file')){
        $filename = $request->file('img_file')->getClientOriginalName();
        $image_thumb = Image::make($request->file('img_file'))->stream();
        $uploaded = Storage::disk('myowndisk')->put($filename , File::get($request->file('img_file')));
        $patient->img = $filename;
      }
      $patient->save();
      if($request->redirectlink==1)
      {
        return redirect('/createreport?patient_id='.$patient->id);
      }else{
        return redirect('/allpatient');
      }
   }
   public function edit($id){
    $currentyear = Date("Y");
    $patient = Patient::find($id);
     return view('psycho_drama/patients/edit',compact('currentyear','patient'));
   }
   public function update(Request $request,$id){
    $patient = Patient::find($id);
    $patient->title = $request->title;
    $patient->name = $request->name;
    $patient->lname = $request->lname;
    $patient->daybirth = $request->daybirth;
    $patient->monthbirth = $request->monthbirth;
    $patient->yearbirth = $request->yearbirth;
    $patient->address= $request->address;
    $patient->postal_code = $request->postal_code;
    $patient->prefectures = $request->prefectures;
    $patient->country = $request->country;
    $patient->email = $request->email;
    $patient->tel = $request->tel;
    if($request->file('img_file')){
        $filename = $request->file('img_file')->getClientOriginalName();
        $image_thumb = Image::make($request->file('img_file'))->stream();
        $uploaded = Storage::disk('myowndisk')->put($filename , File::get($request->file('img_file')));
        $patient->img = $filename;
      }
      $patient->save();
      return redirect('/allpatient');

   }

   public function deleteAll(Request $request)
   {
       $ids = $request->ids;
       Patient::whereIn('id',explode(",",$ids))->delete();
       //return redirect()->back();
       return response()->json(['success'=>"Patient Deleted successfully."]);
   }
  }
