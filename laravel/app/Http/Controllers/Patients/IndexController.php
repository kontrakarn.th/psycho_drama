<?php

namespace App\Http\Controllers\Patients;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Structure;
use Illuminate\Support\Facades\Auth;
use App\ReportType;
use App\Report;
use App\CharatorType;
use App\Charactor;
use App\RelationType;
use App\Relation;
use App\Patient;
use App\Http\Controllers\Controller;
use Session;

class IndexController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth:person');
        $this->middleware('auth');
    }
   public function index(){
    $report =  Patient::get();
    return view('psycho_drama/patients/index',compact('report'));
   }


  }
